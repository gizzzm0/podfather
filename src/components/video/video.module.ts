import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { VideoComponent } from './video';

@NgModule({
  declarations: [
    VideoComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    VideoComponent
  ]
})
export class VideoComponentModule {}
