import { Component, Input } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media';

import { Post } from '../../pages/post/post';

@Component({
  selector: 'video-component',
  templateUrl: 'video.html'
})
export class VideoComponent {
  @Input() item: any;

  options: StreamingVideoOptions = {
    successCallback: () => { console.log('Video played') },
    errorCallback: (e) => { console.log('Error streaming') },
    orientation: 'landscape'
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public streamingMedia: StreamingMedia) {
  }

  playVideo() {
    alert(this.item.src);
    this.streamingMedia.playVideo(this.item.src, this.options);
  }

  likeUnlike(post: any) {
    let isliked: boolean = post.isliked;
    alert(isliked);
    if (isliked) {
      alert('Exec 1');
      if (post.likes > 0) {
        post.isliked = false;
        post.likes--;
      }
    } else {
      alert('Exec 2');
      post.isliked = true;
      post.likes++;
    }
  }

  goToPost(item) {
    this.navCtrl.push(Post, { post: item });
  }

}
