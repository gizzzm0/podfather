import { Component, Input } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Post } from '../../pages/post/post';

@Component({
  selector: 'image-component',
  templateUrl: 'image.html'
})
export class ImageComponent {
  @Input() item: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  likeUnlike(post: any) {
    let isliked: boolean = post.isliked;
    alert(isliked);
    if (isliked == true) {
      alert('Exec 1');
      if (post.likes > 0) {
        post.isliked = false;
        post.likes--;
      }
    } else {
      alert('Exec 2');
      post.isliked = true;
      post.likes++;
    }
  }

  goToPost(item) {
    this.navCtrl.push(Post, { post: item });
  }
}
