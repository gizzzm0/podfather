import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ImageComponent } from './image';

@NgModule({
  declarations: [
    ImageComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    ImageComponent
  ]
})
export class ImageComponentModule {}
