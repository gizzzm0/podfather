import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { AudioComponent } from './audio';

@NgModule({
  declarations: [
    AudioComponent,
  ],
  imports: [
    IonicModule
  ],
  exports: [
    AudioComponent
  ]
})
export class AudioComponentModule { }
