import { Component, Input } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AudioProvider } from 'ionic-audio';

import { Post } from '../../pages/post/post';

@Component({
  selector: 'audio-component',
  templateUrl: 'audio.html'
})
export class AudioComponent {
  @Input() item: any;
  private selectedTrack: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private audioProvider: AudioProvider) {
  }

  likeUnlike(post: any) {
    let isliked: boolean = post.isliked;
    alert(isliked);
    if (isliked == true) {
      alert('Exec 1');
      if (post.likes > 0) {
        post.isliked = false;
        post.likes--;
      }
    } else {
      alert('Exec 2');
      post.isliked = true;
      post.likes++;
    }
  }

  goToPost(item) {
    this.navCtrl.push(Post, { post: item });
  }

  playSelectedTrack() {
    this.audioProvider.play(this.selectedTrack);
  }

  pauseSelectedTrack() {
    this.audioProvider.pause(this.selectedTrack);
  }

  onTrackFinished(track: any) {
    alert('Track finished');
  }
}
