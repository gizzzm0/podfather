import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { PlainTextComponent } from './plain-text';

@NgModule({
  declarations: [
    PlainTextComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    PlainTextComponent
  ]
})
export class PlainTextComponentModule {}
