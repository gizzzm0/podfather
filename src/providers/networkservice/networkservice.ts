import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class NetworkServiceProvider {

  constructor(public http: Http) {
    this.http = http;
  }

  public userRegistration(details) {
    let endpoint: string = 'http://localhost:8080/v1/register';
    let headers: any = new Headers({ 'Content-Type': 'application/json' });
    let options: any = new RequestOptions({ headers: headers });

    return this.http.post(endpoint, JSON.stringify(details), options)
      .map(res => res.json())
      .catch(this.errorHandler);
  }

  public userLogin(credentials) {
    let endpoint: string = 'http://localhost:8080/v1/login';
    let headers: any = new Headers({ 'Content-Type': 'application/json' });
    let options: any = new RequestOptions({ headers: headers });

    return this.http.post(endpoint, JSON.stringify(credentials), options)
      .map(res => res.json())
      .catch(this.errorHandler);
  }

  public getPosts(UID) {
    let endpoint: string = 'http://localhost:8080/v1/posts/user/' + UID;
    let headers: any = new Headers({ 'Content-Type': 'application/json' });
    let options: any = new RequestOptions({ headers: headers });

    return this.http.get(endpoint, options)
      .map(res => res.json())
      .catch(this.errorHandler);
  }

  public getCategories() {
    let endpoint: string = 'http://localhost:8080/v1/posts/categories';
    let headers: any = new Headers({ 'Content-Type': 'application/json' });
    let options: any = new RequestOptions({ headers: headers });

    return this.http.get(endpoint, options)
      .map(res => res.json())
      .catch(this.errorHandler);
  }

  public getCategoryPosts(category) {
    let endpoint: string = 'http://localhost:8080/v1/posts/' + category;
    let headers: any = new Headers({ 'Content-Type': 'application/json' });
    let options: any = new RequestOptions({ headers: headers });

    return this.http.get(endpoint, options)
      .map(res => res.json())
      .catch(this.errorHandler);
  }

  public errorHandler(error) {
    console.error(error);
    return Observable.throw(JSON.stringify(error) || 'Server/Endpoint error');
  }
}
