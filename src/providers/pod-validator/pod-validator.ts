import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/map';


@Injectable()
export class PodValidatorProvider {

  constructor() {
  }

  validateEmail(formControl: FormControl) {
    return new Promise(resolve => {
      let emailExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (!emailExp.test(formControl.value)) {
        resolve({ InvalidEmail: true });
      }
      resolve(null);
    });
  }
}
