import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';

import { NetworkServiceProvider } from '../../providers/networkservice/networkservice';

@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class Category {
  categoryPosts: any;
  pageTitle: string;

  constructor(public navParams: NavParams, public networkService: NetworkServiceProvider) {
    let category = this.navParams.data;
    this.pageTitle = category.title;
    this.loadPosts(category.id);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Category');
  }

  loadPosts(category) {
    this.networkService.getCategoryPosts(category).subscribe(posts => {
      if (posts.length > 0) {
        this.categoryPosts = posts;
      } else {
        alert("Error getting data");
      }
    });
  }

}
