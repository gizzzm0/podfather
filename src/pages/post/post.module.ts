import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Post } from './post';

import { ImageComponent } from '../../components/image/image';
import { AudioComponent } from '../../components/audio/audio';
import { VideoComponent } from '../../components/video/video';
import { PlainTextComponent } from '../../components/plain-text/plain-text';

@NgModule({
  declarations: [
    Post,
    ImageComponent,
    AudioComponent,
    VideoComponent,
    PlainTextComponent
  ],
  imports: [
    IonicPageModule.forChild(Post),
  ],
  exports: [
    Post
  ]
})
export class PostPageModule { }
