import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
})
export class Post {
  private item: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.item = navParams.get('post');
  }

  likeUnlike(item: any) {
    if (item.isliked == true) {
      item.isliked = false;
      item.likes -= 1;
    } else {
      item.isliked = true;
      item.likes += 1;
    }
  }
}
