import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, AlertController, Loading } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PodValidatorProvider } from '../../providers/pod-validator/pod-validator';
import { NetworkServiceProvider } from '../../providers/networkservice/networkservice';
import { Storage } from '@ionic/storage';

import { Timeline } from '../timeline/timeline';

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUp {
  registrationForm: FormGroup;
  isInvalid: boolean = false;
  loader: Loading;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public formBuilder: FormBuilder, public podValidator: PodValidatorProvider, public networkService: NetworkServiceProvider, public storage: Storage) {
    this.registrationForm = formBuilder.group({
      fname: ['', Validators.compose([Validators.required])],
      uname: ['', Validators.compose([Validators.pattern('[a-zA-Z0-9]*'), Validators.required])],
      email: ['', Validators.compose([Validators.maxLength(30), Validators.required]), podValidator.validateEmail],
      pass: ['', Validators.required],
      cpass: ['', Validators.required],
      notify: [false]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUp');
  }

  signUp() {
    if (!this.registrationForm.valid && (this.registrationForm.get('pass').value != this.registrationForm.get('cpass').value)) {
      this.isInvalid = true;
    } else {
      let formData = this.registrationForm.value;
      this.networkService.userRegistration(formData).subscribe(
        token => {
          if (token.success) {
            this.storage.set('loggedIn', true);
            this.storage.set('userID', token.userid);
            this.storage.set('userName', token.username);
            this.storage.set('fullName', token.name);
            this.storage.set('avi', token.avi);
            setTimeout(() => {
              //this.loader.dismiss();
              this.navCtrl.setRoot(Timeline);
            });
          } else {
            alert("Error: " + JSON.stringify(token));
          }
        },
        error => {
          this.showError("Registration Error: " + error);
        }
      );
    }
  }

  showLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Signing you up..."
    });
    this.loader.present();
  }

  showError(text) {
    /*setTimeout(() => {
      this.loader.dismiss();
    });*/
    let alert = this.alertCtrl.create({
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
}
