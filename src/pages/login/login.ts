import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, AlertController, Loading } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NetworkServiceProvider } from '../../providers/networkservice/networkservice';
import { Storage } from '@ionic/storage';

import { PassReset } from '../pass-reset/pass-reset';
import { Timeline } from '../timeline/timeline';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
  loginForm: FormGroup;
  isInvalid: boolean = false;
  loading: Loading;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public formBuilder: FormBuilder, public networkservice: NetworkServiceProvider, public storage: Storage) {
    this.loginForm = formBuilder.group({
      login: ['', Validators.compose([Validators.pattern('[a-zA-Z0-9]*'), Validators.required])],
      password: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  signIn() {
    //this.navCtrl.setRoot(Timeline);
    if (!this.loginForm.valid) {
      alert('Invalid form');
    } else {
      let formData = this.loginForm.value;
      this.networkservice.userLogin(formData).subscribe(
        token => {
          if (token.success) {
            console.log(JSON.stringify(token.user[0]));
            this.storage.set('loggedIn', true);
            this.storage.set('userID', token.user[0].userid);
            this.storage.set('userName', token.user[0].username);
            this.storage.set('fullName', token.user[0].name);
            this.storage.set('avi', token.user[0].avi);
            setTimeout(() => {
              //this.loader.dismiss();
              this.navCtrl.setRoot(Timeline);
            });
          } else {
            alert("Error: " + JSON.stringify(token));
          }
        },
        error => {
          this.showError("Registration Error: " + error);
        }
      );
    }
  }

  passReset() {
    this.navCtrl.push(PassReset);
  }

  socialLogin(network) {
    alert("Signing in to " + network);
  }

  showError(text) {
    /*setTimeout(() => {
      this.loader.dismiss();
    });*/
    let alert = this.alertCtrl.create({
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
}
