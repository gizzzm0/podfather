import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Timeline } from '../timeline/timeline';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.navCtrl.setRoot(Timeline);
    }
      , 4000);
  }

}
