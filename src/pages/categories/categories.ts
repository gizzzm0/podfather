import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { NetworkServiceProvider } from '../../providers/networkservice/networkservice';

import { Category } from '../category/category';

@IonicPage()
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class Categories {

  categories: Array<{ icon: string, title: any }>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public networkService: NetworkServiceProvider) {
    this.loadCategories();
  }

  loadCategories() {
    this.networkService.getCategories().subscribe(res => {
      if (res.length > 0) {
        this.categories = res;
      } else {
        alert("Error getting data");
      }
    });
  }

  navNext(cat) {
    this.navCtrl.push(Category, {
      id: cat.id,
      title: cat.title
    });
  }
}
