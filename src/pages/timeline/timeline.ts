import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { NetworkServiceProvider } from '../../providers/networkservice/networkservice';

@IonicPage()
@Component({
  selector: 'page-timeline',
  templateUrl: 'timeline.html',
})
export class Timeline {
  myTimeline: any;

  constructor(public networkService: NetworkServiceProvider, public storage: Storage) {
    this.loadTimeline();
  }

  ionViewDidLoad() {
  }

  loadTimeline() {
    this.storage.get('userID').then((UID) => {
      this.networkService.getPosts(UID).subscribe(timeline => {
        if (timeline.length > 0) {
          this.myTimeline = timeline;
        } else {
          alert("Error getting data");
        }
      });
    });
  }

}
