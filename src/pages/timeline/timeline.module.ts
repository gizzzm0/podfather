import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Timeline } from './timeline';
import { ImageComponent } from '../../components/image/image';
import { AudioComponent } from '../../components/audio/audio';
import { VideoComponent } from '../../components/video/video';
import { PlainTextComponent } from '../../components/plain-text/plain-text';

@NgModule({
  declarations: [
    Timeline,
    ImageComponent,
    AudioComponent,
    VideoComponent,
    PlainTextComponent
  ],
  imports: [
    IonicPageModule.forChild(Timeline),
  ],
  exports: [
    Timeline
  ]
})
export class TimelineModule { }
