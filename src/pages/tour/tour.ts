import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { NavigationBar } from '@ionic-native/navigation-bar';

import { Start } from '../start/start';

@IonicPage()
@Component({
  selector: 'page-tour',
  templateUrl: 'tour.html',
})
export class Tour {

  constructor(public navCtrl: NavController, public navParams: NavParams, public statusBar: StatusBar, public navigationBar: NavigationBar) {
    //this.statusBar.hide();
    //this.navigationBar.hideNavigationBar();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Tour');
  }

  go() {
    this.navCtrl.setRoot(Start);
  }

}
