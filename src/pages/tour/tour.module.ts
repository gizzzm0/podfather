import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Tour } from './tour';

@NgModule({
  declarations: [
    Tour,
  ],
  imports: [
    IonicPageModule.forChild(Tour),
  ],
  exports: [
    Tour
  ]
})
export class TourModule {}
