import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Login } from '../login/login';
import { SignUp } from '../sign-up/sign-up';
import { Tour } from '../tour/tour';

/**
 * Generated class for the Start page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-start',
  templateUrl: 'start.html',
})
export class Start {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Start');
  }

  goTo(nextPage) {
    if (nextPage == 'login') {
      this.navCtrl.push(Login);
    } else if (nextPage == 'register') {
      this.navCtrl.push(SignUp);
    } else {
      this.navCtrl.push(Tour);
    }
  }

}
