import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PassReset } from './pass-reset';

@NgModule({
  declarations: [
    PassReset,
  ],
  imports: [
    IonicPageModule.forChild(PassReset),
  ],
  exports: [
    PassReset
  ]
})
export class PassResetModule {}
