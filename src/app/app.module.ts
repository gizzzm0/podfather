import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { NavigationBar } from '@ionic-native/navigation-bar';
import { VideoPlayer } from '@ionic-native/video-player';
import { Toast } from '@ionic-native/toast';
import { IonicAudioModule, defaultAudioProviderFactory } from 'ionic-audio';
import { StreamingMedia } from '@ionic-native/streaming-media';
import { VgCoreModule } from 'videogular2/core';
import { VgControlsModule } from 'videogular2/controls';
import { VgOverlayPlayModule } from 'videogular2/overlay-play';
import { VgBufferingModule } from 'videogular2/buffering';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Start } from '../pages/start/start';
import { Login } from '../pages/login/login';
import { SignUp } from '../pages/sign-up/sign-up';
import { PassReset } from '../pages/pass-reset/pass-reset';
import { Tour } from '../pages/tour/tour';
import { Timeline } from '../pages/timeline/timeline';
import { Categories } from '../pages/categories/categories';
import { Category } from '../pages/category/category';
import { Post } from '../pages/post/post';
import { Notifications } from '../pages/notifications/notifications';
import { Settings } from '../pages/settings/settings';
import { VideoComponent } from '../components/video/video';
import { AudioComponent } from '../components/audio/audio';
import { ImageComponent } from '../components/image/image';
import { PlainTextComponent } from '../components/plain-text/plain-text';
import { NetworkServiceProvider } from '../providers/networkservice/networkservice';
import { SqliteServiceProvider } from '../providers/sqliteservice/sqliteservice';
import { PodValidatorProvider } from '../providers/pod-validator/pod-validator';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Start,
    Login,
    SignUp,
    Tour,
    PassReset,
    Timeline,
    Categories,
    Category,
    Post,
    Notifications,
    Settings,
    VideoComponent,
    AudioComponent,
    ImageComponent,
    PlainTextComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    IonicAudioModule.forRoot(defaultAudioProviderFactory)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Start,
    Login,
    SignUp,
    Tour,
    PassReset,
    Timeline,
    Categories,
    Category,
    Post,
    Notifications,
    Settings
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NavigationBar,
    VideoPlayer,
    Toast,
    StreamingMedia,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    NetworkServiceProvider,
    SqliteServiceProvider,
    PodValidatorProvider
  ]
})
export class AppModule { }
