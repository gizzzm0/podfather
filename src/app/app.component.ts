import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { StatusBar } from '@ionic-native/status-bar';
import { NavigationBar } from '@ionic-native/navigation-bar';

import { Start } from '../pages/start/start';
import { HomePage } from '../pages/home/home';
import { Timeline } from '../pages/timeline/timeline';
import { Categories } from '../pages/categories/categories';
import { Notifications } from '../pages/notifications/notifications';
import { Settings } from '../pages/settings/settings';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  pages: Array<{ icon: string, component: any }>;
  isLoggedIn: boolean = false;

  constructor(public platform: Platform, public statusBar: StatusBar, public navigationBar: NavigationBar, public storage: Storage) {
    this.initializeApp();
    this.checkStatus();

    this.pages = [
      { icon: 'paper', component: Timeline },
      { icon: 'apps', component: Categories },
      { icon: 'text', component: Notifications },
      { icon: 'settings', component: Settings },
      { icon: 'power', component: 'Logout' }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      //this.statusBar.backgroundColorByHexString("#ffffff");
      this.statusBar.hide();
      this.navigationBar.setUp(true);
      this.checkStatus();
    });
  }

  checkStatus() {
    this.storage.ready().then(() => {
      this.storage.get('loggedIn').then((result) => {
        if (!result) {
          this.rootPage = Start;
        } else {
          this.isLoggedIn = true;
          this.rootPage = Timeline;
        }
      });
    });
  }

  openPage(page) {
    if (page.component != 'Logout') {
      this.nav.setRoot(page.component);
    } else {
      this.logout();
    }
  }

  logout() {
    this.storage.remove('loggedIn').then(() => {
      this.storage.remove('userID');
      this.storage.remove('userName');
      setTimeout(() => {
        this.nav.setRoot(Start);
      });
    });
  }
}

